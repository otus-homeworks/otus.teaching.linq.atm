﻿using Otus.Teaching.Linq.ATM.Core.Entities;
using System;
using System.Collections.Generic;
using System.Linq;

namespace Otus.Teaching.Linq.ATM.Core.Services
{
    public partial class ATMManager
    {
        public IEnumerable<Account> Accounts { get; private set; }

        public IEnumerable<User> Users { get; private set; }

        public IEnumerable<OperationsHistory> History { get; private set; }

        public ATMManager(IEnumerable<Account> accounts, IEnumerable<User> users, IEnumerable<OperationsHistory> history)
        {
            Accounts = accounts;
            Users = users;
            History = history;
        }

        //TODO: Добавить методы получения данных для банкомата
        public void ShowTasks123()
        {
            Console.WriteLine(" Задания 1,2,3   Вывод информации по логину и паролю пользователя");
            Console.Write(" Введите логин : ");
            string login = Console.ReadLine();
            Console.Write(" Введите пароль : ");
            string password = Console.ReadLine();

            // Чтобы не усложнять, при возможных но нестандартных вещах (нет, или больше одного пользователя с login и т.д.) выкидывается исключение.
            try
            {
                // Задание 1.
                User user = GetUserByLoginAndPassword(login, password);

                // Задание 2.
                var usersAccounts = GetUsersAccountsByLoginAndPassword(login, password);

                // Задание 3.
                var historyOfUserAccounts = GetHistoryOfUserAccounts(login, password);

                if (usersAccounts.Count() > 0)
                {
                    Console.WriteLine($" У пользователя {user.GetInfo()} аккаунтов: {usersAccounts.Count()} ");

                    foreach (Account account in usersAccounts)
                    {
                        Console.WriteLine($"   Открыт {account.OpeningDate} :  остаток {account.CashAll}");
                        var operationsHistory = GetHistoryForAccount(account);
                        foreach (OperationsHistory opHistory in operationsHistory)
                            Console.WriteLine("      " + opHistory.GetInfo());
                    }
                }
                else
                    Console.WriteLine($" У пользователя {user.GetInfo()} не найдено аккаунтов");                
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

        }

        public void ShowTasks4()
        {
            Console.WriteLine("\n Задание 4   Вывод данных о всех операциях пополнения счёта с указанием владельца каждого счёта");

            var historyOfUserAccounts = GetOperationsHistoryOfInpCashWithInfAboutUser();

            foreach (var item in historyOfUserAccounts)
                Console.WriteLine($"Id операции  {item.operationsHistory.Id} , user : {item.user.FirstName}  {item.user.SurName} ");
        }

        public void ShowTasks4prepare()
        {
            // список операций пополнения с userId
            var operationsHistoryWithUserID =
                   from operationsHistory in History
                   where operationsHistory.OperationType == OperationType.InputCash
                   join account in Accounts on operationsHistory.AccountId equals account.Id
                   select new { opHistory = operationsHistory, userId = account.UserId };

            var result =
                from opHistUserId in operationsHistoryWithUserID
                join user in Users on opHistUserId.userId equals user.Id
                select new { opHistUserId.opHistory, aUser = user };


            //var operationsHistoryWithUserID = History.Where((opHist) => opHist.OperationType == OperationType.InputCash)
            //                    .Join(Accounts,
            //                      op => op.AccountId,
            //                      ac => ac.Id,
            //                      (op, ac) => new { opHistory = op, userId = ac.UserId });


            //var result = operationsHistoryWithUserID.Join(Users,
            //    opHistUserId => opHistUserId.userId,
            //    user => user.Id,
            //    (opHistUserId, user) => new { opHistUserId.opHistory, aUser = user });



            //foreach (var item in operationsHistoryWithUserID)
            //    Console.WriteLine( $"Id операции  {item.opHistory.Id} , userId = {item.userId}" );

            foreach (var item in result)
                Console.WriteLine($"Id операции  {item.opHistory.Id} , user : {item.aUser.FirstName}  {item.aUser.SurName} ");
        }

        public void ShowTasks5()
        {
            // Вывод данных о всех пользователях у которых на счёте сумма больше N (N задаётся из вне и может быть любой);
            Console.WriteLine("\n Задание 5   Вывод данных о всех пользователях у которых на счёте сумма больше N");
            Console.Write(" Введите целочисленное N большее или равное 0: ");
            string strN = Console.ReadLine();
            try
            {
                uint thresholdSumm = UInt32.Parse(strN);
                IEnumerable<User> result = GetUserWithSummMoreThan(thresholdSumm);
                if(result.Count()>0)
                    foreach (User user in result)
                        Console.WriteLine($" У пользователя {user.GetInfo()} (userId:{user.Id})");
                else
                    Console.WriteLine($" Нет пользователей с суммой на счёте более {strN}");
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }
        }

        public IEnumerable<User> GetUserWithSummMoreThan(uint thresholdSumm)
        {
            // Работает с любым из вариантов.
            /*
                        return (from user in Users
                                 join account in Accounts
                                 on user.Id equals account.UserId
                                 where account.CashAll > thresholdSumm
                                 select user).Distinct();
            */

            return Users.Join(Accounts.Where(acc => acc.CashAll > thresholdSumm),
                           (us => us.Id),
                           (acc => acc.UserId),
                           (us, acc) => us).Distinct();

        }

        public IEnumerable<OperationHistoryAndUser> GetOperationsHistoryOfInpCashWithInfAboutUser()
        {
            // Работает с любым из вариантов.

            return from opHistUserId in

                       from operationsHistory in History
                       where operationsHistory.OperationType == OperationType.InputCash
                       join account in Accounts on operationsHistory.AccountId equals account.Id
                       select new { opHistory = operationsHistory, userId = account.UserId }

                   join user in Users on opHistUserId.userId equals user.Id
                   select new OperationHistoryAndUser(opHistUserId.opHistory, user);



            //return History.Where((opHist) => opHist.OperationType == OperationType.InputCash)
            //                    .Join(Accounts,
            //                      op => op.AccountId,
            //                      ac => ac.Id,
            //                      (op, ac) => new { opHistory = op, userId = ac.UserId }).
            //                          Join(Users,
            //                                opHistUserId => opHistUserId.userId,
            //                                user => user.Id,
            //                                (opHistUserId, user) => new OperationHistoryAndUser(opHistUserId.opHistory, user)/* { opHistUserId.opHistory, aUser = user }*/
            //                                );

        }

        public IEnumerable<OperationsHistory> GetHistoryOfUserAccounts(string login, string password)
        {
            // Работает с любым из вариантов.

            // Запрос с использованием синтаксиса методов.           
            //return History.Join(
            //                    Users.Where(user => user.Login == login && user.Password == password)
            //                        .Join(Accounts,
            //                            usr => usr.Id,
            //                            acc => acc.UserId,
            //                            (usr, acc) => acc),

            //                    hstr => hstr.AccountId,
            //                    acc => acc.Id,
            //                    (hstr, acc) => hstr);

            // Запрос с декларативным синтаксисом.
            return from hstr in History
                   join acc in
                      from aUser in Users
                      where aUser.Login == login && aUser.Password == password
                      join acc in Accounts on aUser.Id equals acc.UserId
                      select acc
                                           on hstr.AccountId equals acc.Id
                   select hstr;

        }

        public IEnumerable<Account> GetUsersAccountsByLoginAndPassword(string login, string password)
        {
            // Работает с любым из вариантов.

            // Запрос с использованием синтаксиса методов.
            return Users.Where(user => user.Login == login && user.Password == password)
                        .Join(Accounts,
                              usr => usr.Id,
                              acc => acc.UserId,
                              (usr, acc) => acc);


            // Запрос с декларативным синтаксисом.
            //return from aUser in Users
            //        where aUser.Login == login && aUser.Password == password
            //        join acc in Accounts on aUser.Id equals acc.UserId
            //        select acc;

            // А так всё же проще всего, чем уступает варианту с join, производительностью ?
            //return from aUser in Users
            //       where aUser.Login == login && aUser.Password == password
            //       from acc in Accounts
            //       where aUser.Id == acc.UserId
            //       select acc;
        }

        public User GetUserByLoginAndPassword(string login, string password)
        {
            try
            {
                var foundedUser = Users.Where(user => user.Login == login).SingleOrDefault();
                if (foundedUser == null)
                    throw new Exception("Не найден пользователь с логин-м: " + login);

                if (foundedUser.Password != password)
                    throw new Exception("Неверный пароль к логину: " + login);

                return foundedUser;
            }
            // Генерируется если при вызове SingleOrDefault найдено более одного элемента. 
            catch (InvalidOperationException)
            {
                throw new Exception("Найдено более одного пользователя с логин-м: " + login);
            }
        }
    }
}