﻿using System;

namespace Otus.Teaching.Linq.ATM.Core.Entities
{
    public class OperationsHistory
    {
        public int Id { get; set; }
        public DateTime OperationDate { get; set; }
        public OperationType OperationType { get; set; }
        public decimal CashSum { get; set; }
        public int AccountId { get; set; }

        public string GetInfo()
        {
            switch (OperationType)
            {
                case OperationType.InputCash:
                    return ("Пополнение на сумму " + CashSum.ToString() + "   " + OperationDate);

                case OperationType.OutputCash:
                    return ("Снятие на сумму     " + CashSum.ToString() + "   " + OperationDate);

                default:
                    return "Неизвестная операция";
            }
        }
    }
}