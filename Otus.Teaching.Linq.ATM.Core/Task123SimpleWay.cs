﻿using Otus.Teaching.Linq.ATM.Core.Entities;
using System.Collections.Generic;
using System.Linq;


namespace Otus.Teaching.Linq.ATM.Core.Services
{
    public partial class ATMManager
    {
        // Не подходит - надо чтобы всё делалось одним запросом. Ну или в запросах были операции помимо Where.
        public void ShowPoints123_Unsuitable()
        {
            System.Console.WriteLine(" Задания 1,2,3   Вывод информации по логину и паролю пользователя");
            System.Console.Write(" Введите логин : ");
            string login = System.Console.ReadLine();
            System.Console.Write(" Введите пароль : ");
            string password = System.Console.ReadLine();

            // Чтобы не усложнять, при возможных но нестандартных вещах (нет, или больше одного пользователя с login и т.д.) выкидывается исключение.
            try
            {
                User user = GetUserByLoginAndPassword(login, password);

                var foundedAccounts = GetAccountsForUserId(user.Id);

                if (foundedAccounts.Count() > 0)
                {
                    System.Console.WriteLine($" У пользователя {user.GetInfo()} аккаунтов: {foundedAccounts.Count()} ");
                    foreach (Account account in foundedAccounts)
                    {
                        System.Console.WriteLine($"   Открыт {account.OpeningDate} :  остаток {account.CashAll}");
                        var operationsHistory = GetHistoryForAccount(account);
                        foreach (OperationsHistory opHistory in operationsHistory)
                            System.Console.WriteLine("      " + opHistory.GetInfo());
                    }
                }
                else
                    System.Console.WriteLine($" У пользователя {user.GetInfo()} не найдено аккаунтов");

            }
            catch (System.Exception e)
            {
                System.Console.WriteLine(e.Message);
            }
        }

        public IEnumerable<OperationsHistory> GetHistoryForAccount(Account account)
        {
            return History.Where(operationsHistory => operationsHistory.AccountId == account.Id);
        }

        public IEnumerable<Account> GetAccountsForUserId(int UserId)
        {
            return Accounts.Where(account => account.UserId == UserId);
        }
    }
}
