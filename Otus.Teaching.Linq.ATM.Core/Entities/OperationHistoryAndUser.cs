﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Otus.Teaching.Linq.ATM.Core.Entities
{
    public class OperationHistoryAndUser
    {
        public OperationsHistory operationsHistory { get; set; }
        public User user { get; set; }
        public OperationHistoryAndUser(OperationsHistory aOperationsHistory, User aUser )
        {
            operationsHistory = aOperationsHistory;
            user = aUser;
    }
}
}
