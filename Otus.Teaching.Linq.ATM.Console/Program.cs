﻿using Otus.Teaching.Linq.ATM.Core.Services;
using Otus.Teaching.Linq.ATM.DataAccess;
using System.Linq;

namespace Otus.Teaching.Linq.ATM.Console
{
    public class Program
    {
        static void Main(string[] args)
        {
            System.Console.WriteLine("Старт приложения-банкомата...");

            var atmManager = CreateATMManager();

            //TODO: Далее выводим результаты разработанных LINQ запросов
            atmManager.ShowTasks123();

            atmManager.ShowTasks4();

            atmManager.ShowTasks5();

            System.Console.WriteLine("Завершение работы приложения-банкомата...");
            System.Console.WriteLine("Нажмите Enter");
            System.Console.ReadLine();
        }

        public static ATMManager CreateATMManager()
        {
            using var dataContext = new ATMDataContext();
            var users = dataContext.Users.ToList();
            var accounts = dataContext.Accounts.ToList();
            var history = dataContext.History.ToList();

            return new ATMManager(accounts, users, history);
        }
    }
}