using System;
using System.Linq;
using Xunit;

namespace otus.teaching.linq.atm.Tests
{
    public class ATMManagerTestsForSimpleWay
    {
        [Fact]
        public void GetUserIdByLoginAndPasswordForCorrectUserTest()
        {
            // Arrange
            var atmManager = Otus.Teaching.Linq.ATM.Console.Program.CreateATMManager();

            // Act
            Otus.Teaching.Linq.ATM.Core.Entities.User user = atmManager.GetUserByLoginAndPassword("cant", "333");

            // Assert
            Assert.Equal(3, user.Id);
        }

        [Fact]
        public void GetUserIdByLoginAndPasswordForUncorrectUserTest()
        {
            // Arrange
            var atmManager = Otus.Teaching.Linq.ATM.Console.Program.CreateATMManager();

            // Act
            // Assert            
            Exception ex = Assert.Throws<Exception>(() => atmManager.GetUserByLoginAndPassword("cantttt", "333"));
        }

        [Fact]
        public void GetUserIdByLoginAndPasswordForUncorrectUserTest2()
        {
            // Arrange
            var atmManager = Otus.Teaching.Linq.ATM.Console.Program.CreateATMManager();

            // Act
            // Assert            
            Exception ex = Assert.Throws<Exception>(() => atmManager.GetUserByLoginAndPassword("cant", "3333333"));
        }

        [Fact]
        public void GetAccountsForUserIdTest()
        {
            // Arrange
            var atmManager = Otus.Teaching.Linq.ATM.Console.Program.CreateATMManager();

            // Act
            Otus.Teaching.Linq.ATM.Core.Entities.User userCant = atmManager.GetUserByLoginAndPassword("cant", "333");
            Otus.Teaching.Linq.ATM.Core.Entities.User userCop = atmManager.GetUserByLoginAndPassword("cop", "555");

            var accountsCant = atmManager.GetAccountsForUserId(userCant.Id);
            var accountsCop = atmManager.GetAccountsForUserId(userCop.Id);
            int nCant = accountsCant.Count();
            int nCop = accountsCop.Count();

            // Assert     
            Assert.Equal(1, nCant);
            Assert.Equal(6, accountsCant.First().Id);

            Assert.Equal(2, nCop);
            Assert.Equal(8, accountsCop.First().Id);
            Assert.Equal(9, accountsCop.Last().Id);
        }
    }

    public class ATMManagerTests
    {
        // ����� ��� GetUserIdByLoginAndPasswordForCorrectUserTest ���� � ATMManagerTestsForSimleWay, ������������ �� ����.

        [Fact]
        public void GetUsersAccountsByLoginAndPasswordTests()
        {
            // Arrange
            var atmManager = Otus.Teaching.Linq.ATM.Console.Program.CreateATMManager();

            // Act
            var accountsCant = atmManager.GetUsersAccountsByLoginAndPassword("cant", "333");
            var accountsCop = atmManager.GetUsersAccountsByLoginAndPassword("cop", "555");

            // Assert
            Assert.Single(accountsCant);
            Otus.Teaching.Linq.ATM.Core.Entities.Account account = accountsCant.First();
            Assert.Equal(6, account.Id);

            Assert.Equal(2, accountsCop.Count());
            Assert.Equal(8, accountsCop.First().Id);
            Assert.Equal(9, accountsCop.Last().Id);
        }

        [Fact]
        public void GetUsersAccountsByLoginAndPasswordTestsForUncorrecUser()
        {
            // Arrange
            var atmManager = Otus.Teaching.Linq.ATM.Console.Program.CreateATMManager();

            // Act
            var accountsCant = atmManager.GetUsersAccountsByLoginAndPassword("canttt", "333");
            var accountsCop = atmManager.GetUsersAccountsByLoginAndPassword("cop", "555555");

            // Assert
            Assert.Empty(accountsCant);
            Assert.Empty(accountsCop);
        }

        [Fact]
        public void GetHistoryOfUserAccountsTest()
        {
            // Arrange
            var atmManager = Otus.Teaching.Linq.ATM.Console.Program.CreateATMManager();

            // Act
            var operationsHistoryForCant = atmManager.GetHistoryOfUserAccounts("cant", "333");
            var operationsHistoryForCop = atmManager.GetHistoryOfUserAccounts("cop", "555");

            // Assert
            Assert.Empty(operationsHistoryForCant);

            Assert.NotEmpty(operationsHistoryForCop);
            int count = operationsHistoryForCop.Count();
            Assert.Equal(6, count);
            // ��� ������� � ������.
            Assert.Equal(10, operationsHistoryForCop.Skip(1).First().Id);
            Assert.Equal(14, operationsHistoryForCop.Last().Id);
            // ��� �������������� � ������.
            Assert.Equal(13, operationsHistoryForCop.Reverse().Skip(1).First().Id);
            // ������ ������� ��� �������������� � ������. ������������� �� ����� � Skip �� ���� ������.            
            Assert.Equal(13, operationsHistoryForCop.Skip(count - 2).First().Id);
        }

        [Fact]
        public void GetOperationsHistoryOfInpCashWithInfAboutUserTest()
        {
            // Arrange
            var atmManager = Otus.Teaching.Linq.ATM.Console.Program.CreateATMManager();

            // Act
            var historyOfUserAccounts = atmManager.GetOperationsHistoryOfInpCashWithInfAboutUser();

            // Assert            
            Assert.NotEmpty(historyOfUserAccounts);
            int count = historyOfUserAccounts.Count();
            Assert.Equal(10, count);
            // ��� ���������� � ������.
            Assert.Equal(1, historyOfUserAccounts.Skip(3).First().user.Id);
            // ��� ������ � ������.
            Assert.Equal(2, historyOfUserAccounts.Skip(4).First().user.Id);
        }

        [Fact]
        public void GetUserWithSummMoreThanTest()
        {
            // Arrange
            var atmManager = Otus.Teaching.Linq.ATM.Console.Program.CreateATMManager();

            // Act
            var usersWithSummMore1000 = atmManager.GetUserWithSummMoreThan(1000);
            var usersWithSummMore6000 = atmManager.GetUserWithSummMoreThan(6000);
            var usersWithSummMore15000 = atmManager.GetUserWithSummMoreThan(15000);
            var usersWithSummMore140000 = atmManager.GetUserWithSummMoreThan(140000);


            // Assert            
            Assert.NotEmpty(usersWithSummMore1000);
            Assert.NotEmpty(usersWithSummMore6000);
            Assert.NotEmpty(usersWithSummMore15000);
            Assert.Empty(usersWithSummMore140000);

            Assert.Equal(1, usersWithSummMore15000.First().Id);
            Assert.Equal(5, usersWithSummMore15000.Last().Id);
        }
    }
}
